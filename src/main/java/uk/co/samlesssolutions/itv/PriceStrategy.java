package uk.co.samlesssolutions.itv;

import java.util.HashMap;
import java.util.Map;

public class PriceStrategy {

    private Map<String, MultiPriceStrategy> priceStrategys = new HashMap<>();
    
    public PriceStrategy(Map<String, MultiPriceStrategy> priceStrategys) {
        this.priceStrategys  = priceStrategys;
    }
        
    public int getTotal(String sku, int quantity, int unitPrice) {
        
        if (this.priceStrategys.containsKey(sku)) {
            MultiPriceStrategy strategy = this.priceStrategys.get(sku);
            int multiples = quantity / strategy.getQuantity();
            int remainder = quantity % strategy.getQuantity();
            return multiples * strategy.getQuantityPrice() + remainder * unitPrice;
        }
        return quantity * unitPrice;        
    }
}
