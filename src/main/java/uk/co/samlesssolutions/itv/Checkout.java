package uk.co.samlesssolutions.itv;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
/**
 * 
 * Main entry point for the SuperMarketCheckout Library
 * Given the requirements, the offers and products needed to be flexible.
 * Hence we have a SuperMarketStock class to check if we have the SKU stocked
 * and also a PriceStrategy class which applies this stored offers each time 
 * we start a new checkout.
 * 
 * No external libraries, no interfaces for DI, no lambdas for and no streams
 * to keep this as simple as possible.
 * 
 * This code should be easy to maintain whether new to Java or a seasoned programmer.
 * 
 * @author Rashad Mughal
 *
 */
public class Checkout {
    
    private Map<String, Integer> basket = new HashMap<>();
    private SuperMarketStock stock;
    
    public Checkout(SuperMarketStock stock) {
        this.stock = stock;
    }
    
    public int total(PriceStrategy multiPriceStrategy) throws ItemNotStockedException {
        int total = 0;
        for (Entry<String, Integer> each : basket.entrySet()) {
            String sku = each.getKey();
            int totalBought = each.getValue();
            int unitPrice = this.stock.getStockPrice(sku);
            total += multiPriceStrategy.getTotal(sku, totalBought, unitPrice);
        }
        return total;
    }

    public void scan(String sku) throws ItemNotStockedException {
        if (this.stock.isStocked(sku)) {
            if (this.basket.containsKey(sku)) {
                this.basket.put(sku, this.basket.get(sku) + 1); 
            } else {
                this.basket.put(sku, 1);
            }
        } else {
            throw new ItemNotStockedException(sku);
        }
    }
}
