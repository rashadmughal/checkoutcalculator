package uk.co.samlesssolutions.itv;

public class ItemNotStockedException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public ItemNotStockedException(String msg) {
        super(msg);
    }
}
