package uk.co.samlesssolutions.itv;

public class MultiPriceStrategy {
    
    private final String sku;
    private final int quantity;
    private final int quantityPrice;
    
    public MultiPriceStrategy(String sku, int quantity, int quantityPricePence) {
        this.sku = sku;
        this.quantity = quantity;
        this.quantityPrice = quantityPricePence;
    }

    public String getSku() {
        return sku;
    }

    public int getQuantity() {
        return quantity;
    }

    public int getQuantityPrice() {
        return quantityPrice;
    }
}
