package uk.co.samlesssolutions.itv;

import java.util.HashMap;
import java.util.Map;

public class SuperMarketStock {

    private Map<String, Integer> stock = new HashMap<>();
    
    public int getStockPrice(String sku) throws ItemNotStockedException {
        if (this.stock.containsKey(sku)) {
            return this.stock.get(sku);
        }
        throw new ItemNotStockedException(sku);
    }
    
    public boolean isStocked(String sku){
        return this.stock.containsKey(sku);
    }

    public void addStock(String sku, int unitPrice) {
        this.stock.put(sku, unitPrice);
    }
}
