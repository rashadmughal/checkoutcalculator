package uk.co.samlesssolutions.itv;

import org.junit.Test;

import uk.co.samlesssolutions.itv.Checkout;
import uk.co.samlesssolutions.itv.ItemNotStockedException;
import uk.co.samlesssolutions.itv.PriceStrategy;
import uk.co.samlesssolutions.itv.SuperMarketStock;
import uk.co.samlesssolutions.itv.MultiPriceStrategy;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;


public class CheckoutTest {
    /*
     * UnitOfWork_StateUnderTest_ExpectedBehavior
     * Ideally should use testng data provider.
     */
      
    @Test
    public void Total_ScanNothing_ShouldTotal0() throws ItemNotStockedException {
        Checkout checkout = new Checkout(getSuperMarketStock());
        assertEquals(0, checkout.total(getPriceStrategy()));
    }
    
    @Test
    public void Scan_ScanA_ShouldTotal50() throws ItemNotStockedException {
        Checkout checkout = new Checkout(getSuperMarketStock());
        checkout.scan("A");
        assertEquals(50, checkout.total(getPriceStrategy()), 50);
    }
    
    @Test(expected = ItemNotStockedException.class)
    public void Scan_ScanEmptyString_ShouldThrowException() throws ItemNotStockedException {
        Checkout checkout = new Checkout(getSuperMarketStock());
        checkout.scan("");
        checkout.total(getPriceStrategy());
    }
    
    @Test
    public void Scan_ScanABCD_ShouldTotal115() throws ItemNotStockedException {

        Checkout checkout = new Checkout(getSuperMarketStock());
        checkout.scan("A");
        checkout.scan("B");
        checkout.scan("C");
        checkout.scan("D");
        assertEquals(115, checkout.total(getPriceStrategy()));
    }
    
    @Test
    public void Scan_ScanA3Times_ShouldTotal130() throws ItemNotStockedException {

        Checkout checkout = new Checkout(getSuperMarketStock());
        checkout.scan("A");
        checkout.scan("A");
        checkout.scan("A");
        assertEquals(130, checkout.total(getPriceStrategy()));
    }
    
    @Test
    public void Scan_ScanB2Times_ShouldTotal45() throws ItemNotStockedException {

        Checkout checkout = new Checkout(getSuperMarketStock());
        checkout.scan("B");
        checkout.scan("B");
        assertEquals(45, checkout.total(getPriceStrategy()));
    }
    
    @Test
    public void Scan_ScanA3TimeB2Times_ShouldTotal175() throws ItemNotStockedException {

        Checkout checkout = new Checkout(getSuperMarketStock());
        checkout.scan("A");
        checkout.scan("A");
        checkout.scan("A");
        checkout.scan("B");
        checkout.scan("B");
        assertEquals(175, checkout.total(getPriceStrategy()));
    }
    
    @Test
    public void Scan_ScanA4TimeB3Times_ShouldTotal255() throws ItemNotStockedException {

        Checkout checkout = new Checkout(getSuperMarketStock());
        checkout.scan("A");
        checkout.scan("A");
        checkout.scan("A");
        checkout.scan("A");
        checkout.scan("B");
        checkout.scan("B");
        checkout.scan("B");
        assertEquals(255, checkout.total(getPriceStrategy()));
    }
    
    private static PriceStrategy getPriceStrategy() {
        Map<String, MultiPriceStrategy> strategys = new HashMap<>();
        strategys.put("A", new MultiPriceStrategy("A", 3, 130));
        strategys.put("B", new MultiPriceStrategy("B", 2, 45));
        
        PriceStrategy priceStragety =  new PriceStrategy(strategys);
        return priceStragety;
    }
    
    private static SuperMarketStock getSuperMarketStock() {
        SuperMarketStock stock = new SuperMarketStock();
        stock.addStock("A", 50);
        stock.addStock("B", 30);
        stock.addStock("C", 20);
        stock.addStock("D", 15);
        return stock;
    }
}
