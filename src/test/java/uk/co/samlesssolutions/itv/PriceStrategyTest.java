package uk.co.samlesssolutions.itv;

import static org.junit.Assert.*;

import java.util.HashMap;

import org.junit.Test;

public class PriceStrategyTest {

    @Test
    public void getTotal_WithNoOffers_ShouldReturnOriginalPrice() {
        PriceStrategy offers = new PriceStrategy(new HashMap<String, MultiPriceStrategy>());
        assertEquals(10, offers.getTotal("A", 1, 10));
    }
    
    @Test
    public void getTotal_With1OfferAndQtyLessThanMinQty_ShouldReturnOriginalPrice() {
        HashMap<String, MultiPriceStrategy> allOffers = new HashMap<String, MultiPriceStrategy>();
        allOffers.put("A", new MultiPriceStrategy("A", 3, 20));
        PriceStrategy offers = new PriceStrategy(allOffers);
        assertEquals(20, offers.getTotal("A", 2, 10));
    }
    
    @Test
    public void getTotal_With1OfferAndQtyEqualToMinQty_ShouldReturnOfferPrice() {
        HashMap<String, MultiPriceStrategy> allOffers = new HashMap<String, MultiPriceStrategy>();
        allOffers.put("A", new MultiPriceStrategy("A", 3, 25));
        PriceStrategy offers = new PriceStrategy(allOffers);
        assertEquals(25, offers.getTotal("A", 3, 10));
    }
    
    @Test
    public void getTotal_With1OfferAndQtyGreaterToMinQty_ShouldReturnCorrectPrice() {
        HashMap<String, MultiPriceStrategy> allOffers = new HashMap<String, MultiPriceStrategy>();
        allOffers.put("A", new MultiPriceStrategy("A", 3, 25));
        PriceStrategy offers = new PriceStrategy(allOffers);
        assertEquals(35, offers.getTotal("A", 4, 10));
    }
    
    @Test
    public void getTotal_With2OffersAndQtyGreaterToMinQty_ShouldReturnCorrectPrice() {
        HashMap<String, MultiPriceStrategy> allOffers = new HashMap<String, MultiPriceStrategy>();
        allOffers.put("A", new MultiPriceStrategy("A", 3, 25));
        allOffers.put("B", new MultiPriceStrategy("B", 2, 50));
        PriceStrategy offers = new PriceStrategy(allOffers);
        assertEquals(100, offers.getTotal("B", 4, 30));
    }

}
