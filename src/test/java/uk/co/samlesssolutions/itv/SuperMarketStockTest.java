package uk.co.samlesssolutions.itv;

import static org.junit.Assert.*;

import org.junit.Test;

import uk.co.samlesssolutions.itv.ItemNotStockedException;
import uk.co.samlesssolutions.itv.SuperMarketStock;

public class SuperMarketStockTest {

    @Test(expected = ItemNotStockedException.class)
    public void getStockPrice_WithNoStock_ShouldThrowException() throws ItemNotStockedException {
        SuperMarketStock stock = new SuperMarketStock();
        stock.getStockPrice("A");
    }
    
    @Test
    public void getStockPrice_WithAStock_ShouldReturnPrice() throws ItemNotStockedException {
        SuperMarketStock stock = new SuperMarketStock();
        stock.addStock("A",10);
        assertEquals(10, stock.getStockPrice("A"));
    }
    
    @Test
    public void isStocked_WithNoStock_ShouldReturnFalse() throws ItemNotStockedException {
        SuperMarketStock stock = new SuperMarketStock();
        stock.isStocked("A");
        assertEquals(false, stock.isStocked("A"));
    }
    
    @Test
    public void isStocked_WithAStock_ShouldReturnTrue() throws ItemNotStockedException {
        SuperMarketStock stock = new SuperMarketStock();
        stock.addStock("A",10);
        assertEquals(true, stock.isStocked("A"));
    }
    
    @Test
    public void addStock_WithAStock_ShouldReturnPriceOfA() throws ItemNotStockedException {
        SuperMarketStock stock = new SuperMarketStock();
        stock.addStock("A", 15);
        assertEquals(15, stock.getStockPrice("A"));
    }
}
